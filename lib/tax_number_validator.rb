# frozen_string_literal: true

require 'active_support/all'
require 'social_security_number'

require_relative 'tax_number_validator/version'
require_relative 'tax_number_validator/constants'
require_relative 'tax_number_validator/errors'
require_relative 'tax_number_validator/caller'
require_relative 'tax_number_validator/personal_code/base'
require_relative 'tax_number_validator/company_code/base'
Dir["#{__dir__}/tax_number_validator/personal_code/*.rb"].sort.each { |f| require f }
Dir["#{__dir__}/tax_number_validator/company_code/*.rb"].sort.each { |f| require f }

module TaxNumberValidator
  autoload :Caller, 'tax_number_validator/caller'

  def self.call(type:, country_code:, number:, birth_date: nil, gender: nil)
    Caller.new(
      type: type,
      number: number,
      country_code: country_code,
      birth_date: birth_date,
      gender: gender
    ).call
  end
end
