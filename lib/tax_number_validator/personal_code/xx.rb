# frozen_string_literal: true

module TaxNumberValidator
  module PersonalCode
    class XX < Base
      REGEX = Regexp.compile('^[\-\\\/\(\)_\.A-Z0-9]{6,20}$')

      def call
        !!matching
      end
    end
  end
end
