# frozen_string_literal: true

module TaxNumberValidator
  module PersonalCode
    class EU < Base
      REGEX = Regexp.compile('^[\-\\\/\(\)_\.A-Z0-9]{10,16}$')

      def call
        !!matching
      end
    end
  end
end
