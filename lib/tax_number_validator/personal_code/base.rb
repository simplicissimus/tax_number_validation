# frozen_string_literal: true

module TaxNumberValidator
  module PersonalCode
    class Base
      def self.call(number:, country_code:, birth_date: nil, gender: nil)
        new(
          number: number,
          country_code: country_code,
          birth_date: birth_date,
          gender: gender
        ).call
      end

      def initialize(number:, country_code:, birth_date: nil, gender: nil)
        @number = number
        @country_code = country_code
        @birth_date = birth_date
        @gender = gender
      end

      attr_reader :number, :country_code, :birth_date, :gender

      def call
        SocialSecurityNumber::Validator.new(
          number: number,
          country_code: country_code,
          birth_date: birth_date,
          gender: gender
        ).valid?
      end

      private

      def common_params
        {
          number: number,
          country_code: country_code,
          gender: gender,
          birth_date: birth_date
        }
      end

      def matching
        @matching ||= number.match(self.class::REGEX)
      end
    end
  end
end
