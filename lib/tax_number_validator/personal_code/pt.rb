# frozen_string_literal: true

module TaxNumberValidator
  module PersonalCode
    class PT < Base
      REGEX = Regexp.compile('^[0-9]{9}$')

      def call
        !!matching && check
      end

      private

      # rule according to:
      # https://pt.wikipedia.org/wiki/N%C3%BAmero_de_identifica%C3%A7%C3%A3o_fiscal
      def check
        digits = number.to_i.digits.reverse
        return false unless digits.size == 9

        mod11 = (0..7).sum { |n| digits[n] * (9 - n) } % 11
        digits[8] == (mod11 < 2 ? 0 : 11 - mod11)
      end
    end
  end
end
