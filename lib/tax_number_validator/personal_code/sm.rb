# frozen_string_literal: true

module TaxNumberValidator
  module PersonalCode
    class SM < Base
      REGEX = Regexp.compile('^\d{1,6}$')

      def call
        !!matching
      end
    end
  end
end
