# frozen_string_literal: true

module TaxNumberValidator
  module PersonalCode
    class IT < Base

      REGEX = Regexp.compile('^(IT)?([0-9]{11})$')

      def call
        super || !!matching
      end

    end
  end
end
