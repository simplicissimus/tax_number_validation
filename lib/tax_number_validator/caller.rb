# frozen_string_literal: true

module TaxNumberValidator
  class Caller
    UNCOVERED_EURO_COUNTRY_CODES = %i[at be bg cy cz dk ee el gr
                                      fr hu ie lt lu lv mt nl pl
                                      ro se si sk fi].freeze

    def self.call(type:, country_code:, number:, birth_date: nil, gender: nil)
      new(
        type: type,
        number: number,
        country_code: country_code,
        birth_date: birth_date,
        gender: gender
      ).call
    end

    def initialize(params)
      @number = params.fetch(:number).to_s.upcase.delete(" \t\r\n")
      @type = params.fetch(:type)
      @country_code = params.fetch(:country_code).to_s.downcase.to_sym
      country_code_converter
      @birth_date = params.fetch(:birth_date)
      @gender = params.fetch(:gender)
    end

    attr_reader :type, :number, :country_code, :birth_date, :gender

    def call
      klass.call(
        number: number,
        country_code: country_code,
        birth_date: birth_date,
        gender: gender
      )
    end

    private

    def klass
      "TaxNumberValidator::#{type.to_s.camelize}Code::#{country_code.upcase}"
        .constantize
    end

    def country_code_converter
      klass
    rescue NameError
      @country_code = if UNCOVERED_EURO_COUNTRY_CODES.include?(country_code)
                        :eu
                      else
                        :xx
                      end
    end
  end
end
