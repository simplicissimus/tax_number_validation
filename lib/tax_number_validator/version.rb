# frozen_string_literal: true

module TaxNumberValidator
  # :nodoc:
  VERSION = '0.6.0'
end
