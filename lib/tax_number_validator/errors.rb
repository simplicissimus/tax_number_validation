# frozen_string_literal: true

module TaxNumberValidator
  # Provides namespace for error classes.
  module Errors
  end
end
