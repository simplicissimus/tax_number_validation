# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class IT < Base
      REGEX = Regexp.compile('^(IT)?([0-9]{11})$')

      def call
        !!matching && logical_validation?
      end

      private

      def logical_validation?
        odds = []
        evens = []
        numerical_part[0..9].split('').map(&:to_i).each_with_index { |e, i| (i + 1).odd? ? odds << e : evens << e }
        x = odds.inject(0) { |sum, d| sum + d }
        y = 2 * (evens.inject(0) { |sum, d| sum + d })
        z = evens.select { |d| d >= 5 }.size
        t = (x + y + z) % 10
        numerical_part[10].chr.to_i == (10 - t) % 10
      end

      def numerical_part
        matching[2]
      end
    end
  end
end
