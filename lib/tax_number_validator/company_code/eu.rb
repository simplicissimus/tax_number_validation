# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class EU < Base
      REGEX = Regexp.compile([
        '^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|',
        '(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|',
        '(EL|GR)?[0-9]{9}|(FI)?[0-9]{8}|',
        '(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(LT)?([0-9]{9}|[0-9]{12})|',
        '(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|',
        '(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(RO)?[0-9]{2,10}|',
        '(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$'
      ].join)

      def call
        !!matching
      end
    end
  end
end
