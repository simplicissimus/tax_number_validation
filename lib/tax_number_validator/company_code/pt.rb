# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class PT < Base
      REGEX = Regexp.compile('^(PT)?[0-9]{9}$')

      def call
        !!matching
      end
    end
  end
end
