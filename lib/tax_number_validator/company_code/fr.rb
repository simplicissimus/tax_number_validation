# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class FR < Base
      REGEX = Regexp.compile('^(FR)?[0-9A-Z]{0,5}[0-9]{9}$')

      def call
        !!matching
      end
    end
  end
end
