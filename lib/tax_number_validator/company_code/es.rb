# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class ES < Base
      REGEX = Regexp.compile('^(ES-?)?([0-9A-Z][0-9]{7}[A-Z])|([A-Z][0-9]{7}[0-9A-Z])$')

      def call
        !!matching
      end
    end
  end
end
