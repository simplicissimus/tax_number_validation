# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class GB < Base
      REGEX = Regexp.compile('^(GB)?([0-9]{9}|[0-9]{12})$')

      def call
        !!matching
      end
    end
  end
end
