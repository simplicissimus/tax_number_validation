# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class SM < Base
      REGEX = Regexp.compile('^SM\d{1,5}$')

      def call
        !!matching
      end
    end
  end
end
