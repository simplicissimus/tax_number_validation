# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class Base
      def self.call(params)
        new(params).call
      end

      def initialize(params)
        @number = params.fetch(:number)
        @country_code = params.fetch(:country_code)
      end

      attr_reader :number, :country_code

      def call
        raise NotImplementedError
      end

      private

      def common_params
        {
          number: number,
          country_code: country_code
        }
      end

      def matching
        @matching ||= number.match(self.class::REGEX)
      end
    end
  end
end
