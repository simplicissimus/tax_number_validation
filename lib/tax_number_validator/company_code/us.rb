# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class US < Base
      def call
        SocialSecurityNumber::Validator.new(
          common_params.merge(country_code: 'us')
        ).valid?
      end
    end
  end
end
