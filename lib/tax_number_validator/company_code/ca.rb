# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class CA < Base
      REGEX = Regexp.compile('^([0-9]{9})(RP|RM|RC|RR|RZ)([0-9]{4})$')

      def call
        !!matching
      end
    end
  end
end
