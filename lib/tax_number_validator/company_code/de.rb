# frozen_string_literal: true

module TaxNumberValidator
  module CompanyCode
    class DE < Base
      REGEX = Regexp.compile('^(DE)?([0-9]{9})$')

      def call
        !!matching
      end
    end
  end
end
