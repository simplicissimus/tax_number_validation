# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'other World countries (with generic validations)' do
    let(:contextual_params) { { type: 'personal', country_code: 'xx' } }
    let(:random_valid_number) { rand(6..16).times.inject('') { |sum, _i| sum + rand(1..9).to_s } }
    let(:random_invalid_number) { rand(2..5).times.inject('') { |sum, _i| sum + rand(1..9).to_s } }
    context 'personal' do
      context 'Japan (jp)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: '123456789012345678903' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Brazil (br)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: '231.002.999-00' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Russia (ru)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'India (in)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Australia (au)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'South Africa (za)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
    end
  end
end
