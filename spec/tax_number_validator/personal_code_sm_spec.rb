# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'San Marino (SM)' do
    let(:contextual_params) { { type: type, country_code: 'sm' } }
    context 'personal' do
      let(:type) { 'personal' }

      it 'returns true when CF is valid' do
        expect(
          TaxNumberValidator.call({ number: '1234' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when CF is not valid' do
        expect(
          TaxNumberValidator.call({ number: 'MRA00A01F839E' }.merge(contextual_params))
        ).to be_falsey
        expect(
          TaxNumberValidator.call({ number: '12a' }.merge(contextual_params))
        ).to be_falsey
        expect(
          TaxNumberValidator.call({ number: 'abc' }.merge(contextual_params))
        ).to be_falsey
      end

    end
  end
end
