# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'United Kingdom (gb)' do
    let(:contextual_params) { { type: type, country_code: 'gb' } }
    context 'company' do
      let(:type) { 'company' }
      it 'returns true when VAT Reg No. is valid with 9 digits' do
        expect(
          described_class.({ number: '735248369' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when VAT Reg No. is valid with 12 digits' do
        expect(
          described_class.({ number: '735248413825' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when VAT Reg No. is valid with prefix country code' do
        expect(
          described_class.({ number: 'GB735248413825' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when VAT Reg No. is valid with some white spaces' do
        expect(
          described_class.({ number: 'GB 735246369' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when VAT Reg No. is not valid' do
        expect(
          described_class.({ number: '4348394934' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
