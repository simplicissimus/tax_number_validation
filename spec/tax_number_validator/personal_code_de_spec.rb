# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'Germany (de)' do
    let(:contextual_params) { { type: 'personal', country_code: 'de' } }
    context 'personal' do
      it 'returns true when Steuer-IdNr is valid with Bundesschema' do
        expect(
          TaxNumberValidator.({ number: '98273204063' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when Steuer-IdNr is valid with some white spaces' do
        expect(
          TaxNumberValidator.({ number: '72 107 869 440' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when Steuer-IdNr is valid with birth date provided' do
        # skip 'analize if birth_date is used'
        expect(
          described_class.({ number: '10 846 345 470',
                             birth_date: '1950-12-01' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when Steuer-IdNr is not valid' do
        expect(
          TaxNumberValidator.({ number: '1084345470' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
