# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'spanish (es)' do
    let(:contextual_params) { { type: type, country_code: 'es' } }
    context 'personal' do
      let(:type) { 'personal' }
      it 'returns true when DNI is valid' do
        expect(
          TaxNumberValidator.({ number: '65004204-V' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when DNI is valid with some white spaces' do
        expect(
          TaxNumberValidator.({ number: '650 04204-V' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when DNI is valid with birth date matching' do
        skip 'analize if birth_date is used'
        expect(
          described_class.({ number: '65004204-V',
                             birth_date: '1950-12-01' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when DNI is not valid' do
        expect(
          TaxNumberValidator.({ number: '65004204' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
