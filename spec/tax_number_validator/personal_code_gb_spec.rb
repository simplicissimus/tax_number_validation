# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'United Kingdom (gb)' do
    let(:contextual_params) { { type: 'personal', country_code: 'gb' } }
    context 'personal' do
      it 'returns true when NINO is valid' do
        expect(
          TaxNumberValidator.({ number: 'ZH319470A' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NINO is valid with some white spaces' do
        expect(
          TaxNumberValidator.({ number: 'ZH 319470 A' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NINO is valid with an associated bith date' do
        expect(
          described_class.({ number: 'RP 79 01 44 B',
                             birth_date: '1976-03-19' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when NINO is not valid' do
        expect(
          TaxNumberValidator.({ number: 'KJ286081' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
