# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'Portugal (pt)' do
    let(:contextual_params) { { type: 'company', country_code: 'pt' } }
    context 'company' do
      it 'returns true when NIPC is valid' do
        expect(
          TaxNumberValidator.({ number: '565004204' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NIPC is valid (with country code)' do
        expect(
          TaxNumberValidator.({ number: 'PT565004204' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NIPC is valid with some white spaces' do
        expect(
          TaxNumberValidator.({ number: '5650 04204' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when NIPC is not valid' do
        expect(
          TaxNumberValidator.({ number: '25004204' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
