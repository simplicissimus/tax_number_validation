# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'Germany (de)' do
    let(:contextual_params) { { type: 'company', country_code: 'de' } }
    context 'company' do
      it 'returns true when USt-IdNr is valid with 9 digits' do
        expect(
          described_class.call({ number: '288129277' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when USt-IdNr is valid with prefix country code' do
        expect(
          described_class.call({ number: 'DE301430300' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when USt-IdNr is valid with some white spaces' do
        expect(
          described_class.call({ number: 'DE 29 4163 189' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when USt-IdNr is not valid' do
        expect(
          described_class.call({ number: 'DE29416189' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
