# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'other European countries (with mild validations)' do
    let(:contextual_params) { { type: 'company', country_code: 'eu' } }
    context 'company' do
      context 'Austria (at)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'ATU12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'ATU1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Belgium (be)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'BE0123456789' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'BE01234678' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Bulgaria (bg)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'BG123456789' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'BG1234567890' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'BG12367890' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Cyprus (cy)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'CY12345678L' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'CY12345678' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Czechia (cz)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'CZ12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'CZ1234567890' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'CZ1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Denmark (dk)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'DK12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'DK1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Denmark (dk)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'DK12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'DK1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Estonia (ee)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'EE123456789' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'EE12345678' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Greece (gr)(el)' do
        it 'returns true when Number is valid with (el)' do
          expect(
            described_class.({ number: 'EL123456789' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns true when Number is valid with (gr)' do
          expect(
            described_class.({ number: 'GR123456789' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'GR1234567890' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Finland (fi)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'FI12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'FI1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Hungary (hu)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'HU12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'HU1234567890' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context '	Ireland (ie)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'IE4S12345L' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'IE412345L' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context '	Lithuania (lt)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'LT123456789' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'LT123456789012' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'LT1234567890' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Luxembourg (lu)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'LU12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'LU1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Latvia (lv)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'LV12345678901' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'LV1234567890' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Malta (mt)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'MT12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'MT1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Netherlands (nl)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'NL123456789B43' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'NL12345678943' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Poland (pl)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'PL1234567890' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'PL12345678901' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Romania (ro)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'RO12' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'RO1234' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'RO1234567890' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'RO123456789012' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Sweden (se)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'SE123456789012' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'SE1234567890' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Slovenia (si)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'SI12345678' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'SI1234567' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Slovakia (sk)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: 'SK1234567890' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: 'SK12345678901' }.merge(contextual_params))
          ).to be_falsey
        end
      end
    end
  end
end
