# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'italian (it)' do
    let(:contextual_params) { { type: type, country_code: 'it' } }
    context 'company' do
      let(:type) { 'company' }
      it 'returns true when P.IVA is valid' do
        expect(
          described_class.({ number: '06363391001' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when P.IVA is valid with prefix country code' do
        expect(
          described_class.({ number: 'IT06363391001' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when P.IVA is valid with some white spaces' do
        expect(
          described_class.({ number: 'IT0636339 1001 ' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when P.IVA is not valid' do
        expect(
          described_class.({ number: '4348394934' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
