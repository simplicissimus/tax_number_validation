# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'France (fr)' do
    let(:contextual_params) { { type: 'company', country_code: 'fr' } }
    context 'company' do
      it 'returns true when SIRET-NIC is valid with 9 digits' do
        expect(
          described_class.call({ number: '808316004' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when SIRET-NIC is valid with prefix country code' do
        expect(
          described_class.call({ number: 'FR808316004' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when SIRET-NIC is valid with 15 digits' do
        expect(
          described_class.call({ number: '80831600400015' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when SIRET-NIC is valid with some white spaces' do
        expect(
          described_class.call({ number: '808316004 00015' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when SIRET-NIC is not valid' do
        expect(
          described_class.call({ number: 'FR29416189' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
