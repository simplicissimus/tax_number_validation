# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'other European countries (with mild validations)' do
    let(:contextual_params) { { type: 'personal', country_code: 'eu' } }
    let(:random_valid_number) { rand(10..16).times.inject('') { |sum, _i| sum + rand(1..9).to_s } }
    let(:random_invalid_number) { rand(8..9).times.inject('') { |sum, _i| sum + rand(1..9).to_s } }
    context 'personal' do
      context 'Austria (at)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Belgium (be)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Bulgaria (bg)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Cyprus (cy)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Czechia (cz)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Denmark (dk)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Denmark (dk)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Estonia (ee)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Greece (gr)(el)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Finland (fi)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'France (fr)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Hungary (hu)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context '	Ireland (ie)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context '	Lithuania (lt)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Luxembourg (lu)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Latvia (lv)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Malta (mt)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Netherlands (nl)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Poland (pl)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Romania (ro)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Sweden (se)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Slovenia (si)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Slovakia (sk)' do
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
    end
  end
end
