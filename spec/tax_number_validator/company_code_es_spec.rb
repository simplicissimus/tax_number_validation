# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'Spain (es)' do
    let(:contextual_params) { { type: 'company', country_code: 'es' } }
    context 'company' do
      it 'returns true when NIF is valid with 8 digits' do
        expect(
          described_class.({ number: 'ESJ12345678' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NIF is valid with 7 digits' do
        expect(
          described_class.({ number: 'ES12345678F' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NIF is valid with 8 digits without prefix ES' do
        expect(
          described_class.({ number: 'A12345678' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NIF is valid with some white spaces' do
        expect(
          described_class.({ number: 'ES H12345678' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when NIF is not valid' do
        expect(
          described_class.({ number: 'ES12345' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
