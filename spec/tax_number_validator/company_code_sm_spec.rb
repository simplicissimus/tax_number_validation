# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'San Marino (SM)' do
    let(:contextual_params) { { type: 'company', country_code: 'sm' } }
    context 'company' do
      it 'returns true when COE is valid' do
        expect(
          TaxNumberValidator.({ number: 'SM12345' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when COE is valid (with less 5 chars)' do
        expect(
          TaxNumberValidator.({ number: 'SM123' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when COE is valid with some zero prefix' do
        expect(
          TaxNumberValidator.({ number: 'SM00123' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when COE is oversized' do
        expect(
          TaxNumberValidator.({ number: 'SM123456' }.merge(contextual_params))
        ).to be_falsey
      end
      it 'returns false when COE is without numbers' do
        expect(
          TaxNumberValidator.({ number: 'SMabcd' }.merge(contextual_params))
        ).to be_falsey
      end
      it 'returns false when COE is without prefix' do
        expect(
          TaxNumberValidator.({ number: '1234' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
