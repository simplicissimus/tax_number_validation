# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'other World countries (with generic validations)' do
    let(:type) { 'company' }
    let(:random_valid_number) { rand(6..18).times.inject('') { |sum, _i| sum + rand(1..9).to_s } }
    let(:random_invalid_number) { rand(2..5).times.inject('') { |sum, _i| sum + rand(1..9).to_s } }
    context 'company' do
      context 'Japan (jp)' do
        let(:contextual_params) { { type: type, country_code: 'jp' } }
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Brazil (br)' do
        let(:contextual_params) { { type: type, country_code: 'br' } }
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: '13.339.532/0001-09' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Russia (ru)' do
        let(:contextual_params) { { type: type, country_code: 'ru' } }
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'India (in)' do
        let(:contextual_params) { { type: type, country_code: 'in' } }
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'Australia (au)' do
        let(:contextual_params) { { type: type, country_code: 'au' } }
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'South Africa (za)' do
        let(:contextual_params) { { type: type, country_code: 'za' } }
        it 'returns true when Number is valid' do
          expect(
            described_class.({ number: random_valid_number }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when Number is not valid' do
          expect(
            described_class.({ number: random_invalid_number }.merge(contextual_params))
          ).to be_falsey
        end
      end
    end
  end
end
