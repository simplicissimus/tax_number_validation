# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'american (us)' do
    let(:contextual_params) { { type: 'company', country_code: 'us' } }
    context 'company' do
      it 'returns true when ITIN is valid' do
        expect(
          described_class.({ number: '998-95-8426' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when EIN is valid' do
        expect(
          described_class.({ number: '53-6359258' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when ITIN is not valid' do
        expect(
          described_class.({ number: '70-94-3207' }.merge(contextual_params))
        ).to be_falsey
      end
      it 'returns false when EIN is not valid' do
        expect(
          described_class.({ number: '53-635D258' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
