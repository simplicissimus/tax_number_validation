# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'american (us)' do
    let(:contextual_params) { { type: 'personal', country_code: 'us' } }
    context 'personal' do
      let(:type) { 'personal' }
      it 'returns true when SSN is valid' do
        expect(
          described_class.({ number: '219-09-9999' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when SSN is not valid' do
        expect(
          described_class.({ number: '219-099999' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
