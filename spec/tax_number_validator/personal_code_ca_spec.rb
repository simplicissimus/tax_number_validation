# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'Canada (ca)' do
    let(:contextual_params) { { type: 'personal', country_code: 'ca' } }
    context 'personal' do
      it 'returns true when SIN is valid' do
        expect(
          described_class.({ number: '130692544' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when SIN is valid with some white spaces' do
        expect(
          TaxNumberValidator.({ number: '130 692 544' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when SIN is valid if also birth date is provided' do
        expect(
          described_class.({ number: '130692544',
                             birth_date: '1950-12-01' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when SIN is not valid' do
        expect(
          described_class.({ number: '13069244' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
