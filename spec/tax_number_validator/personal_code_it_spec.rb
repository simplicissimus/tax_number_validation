# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'italian (it)' do
    let(:contextual_params) { { type: type, country_code: 'it' } }
    context 'personal' do
      let(:type) { 'personal' }
      context 'alpha' do
        it 'returns true when CF is valid' do
          expect(
            TaxNumberValidator.call({ number: 'PTTLCU82P16F839E' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns true when CF is valid with birth date matching' do
          expect(
            described_class.call({ number: 'RSSMRO76C19C890O',
                                   birth_date: '1976-03-19' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when CF is not valid' do
          expect(
            TaxNumberValidator.call({ number: 'MRA00A01F839E' }.merge(contextual_params))
          ).to be_falsey
          expect(
            TaxNumberValidator.call({ number: 'RS1MRO76C19C890O' }.merge(contextual_params))
          ).to be_falsey
          expect(
            TaxNumberValidator.call({ number: 'RSSMRO76519C890O' }.merge(contextual_params))
          ).to be_falsey
        end
      end
      context 'numeric' do
        it 'returns true when CF is numerical 11 digits valid' do
          expect(
            TaxNumberValidator.({ number: '12345678924' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when CF is not 11 digits valid' do
          expect(
            TaxNumberValidator.({ number: '1234567892' }.merge(contextual_params))
          ).to be_falsey
          expect(
            TaxNumberValidator.({ number: '12345A67892' }.merge(contextual_params))
          ).to be_falsey
        end
        it 'returns true when CF has 11 digits with prefix alpha' do
          expect(
            TaxNumberValidator.({ number: 'IT12345678924' }.merge(contextual_params))
          ).to be_truthy
        end
        it 'returns false when CF has 11 digits with wrong prefix alpha' do
          expect(
            TaxNumberValidator.({ number: 'FR12345678924' }.merge(contextual_params))
          ).to be_falsey
        end
      end
    end
  end
end
