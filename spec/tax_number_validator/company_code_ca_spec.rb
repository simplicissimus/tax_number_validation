# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'Canada (ca)' do
    let(:contextual_params) { { type: 'company', country_code: 'ca' } }
    context 'company' do
      it 'returns true when BN is valid' do
        expect(
          described_class.call({ number: '112233445RP0001' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when BN is valid with whitespaces' do
        expect(
          described_class.call({ number: '112233245 RM 0301' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when BN is not valid (business number mismatch)' do
        expect(
          described_class.call({ number: '11233245 RR 0601' }.merge(contextual_params))
        ).to be_falsey
      end
      it 'returns false when BN is not valid (program identifier letter mismatch)' do
        expect(
          described_class.call({ number: '112233245 XX 0301' }.merge(contextual_params))
        ).to be_falsey
      end
      it 'returns false when BN is not valid (specific account number mismatch)' do
        expect(
          described_class.call({ number: '112233245 RM 701' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
