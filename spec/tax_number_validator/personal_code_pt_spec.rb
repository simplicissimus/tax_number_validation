# frozen_string_literal: true

require 'tax_number_validator'

RSpec.describe TaxNumberValidator do
  context 'Portugal (pt)' do
    let(:contextual_params) { { type: 'personal', country_code: 'pt' } }
    context 'personal' do
      it 'returns true when NIF is valid' do
        expect(
          TaxNumberValidator.({ number: '565004204' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NIF is valid with some white spaces' do
        expect(
          TaxNumberValidator.({ number: '5650 04204' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns true when NIF is valid if also birth date is provided' do
        expect(
          described_class.({ number: '565004204',
                             birth_date: '1950-12-01' }.merge(contextual_params))
        ).to be_truthy
      end
      it 'returns false when NIF is not valid' do
        expect(
          TaxNumberValidator.({ number: '565004904' }.merge(contextual_params))
        ).to be_falsey
      end
    end
  end
end
