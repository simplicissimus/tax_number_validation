# frozen_string_literal: true

module RspecHelpers
  include TaxNumberValidator::Constants

  def random_token
    SecureRandom.hex(30)
  end
end
