# frozen_string_literal: true

RSpec.describe TaxNumberValidator do
  it 'has a version number' do
    expect(TaxNumberValidator::VERSION).to be '0.6.0'
  end
end
