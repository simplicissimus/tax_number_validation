# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tax_number_validator/version'

Gem::Specification.new do |spec|
  spec.name          = 'tax_number_validator'
  spec.version       = TaxNumberValidator::VERSION
  spec.authors       = ['Mirco Frison']
  spec.email         = ['mirco.frison@streetlib.com']

  spec.summary       = 'Tax Number Validator'
  spec.description   = 'Tax number validator for individual and company typologies in different countries'
  spec.homepage      = 'https://bitbucket.org/simplicissimus/tax_number_validation'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0")
                     .reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.4'
  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.63'
  spec.add_dependency 'activesupport'
  spec.add_dependency 'social_security_number', '0.1.12'
end
