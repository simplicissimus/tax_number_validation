# TaxNumberValidator

A standalone module to check validity for personal (individual) or company tax number
in following countries:

  - Italy (C.F. and P.IVA)
  - USA (SSN, ITIN and EIN)
  - United Kingdom (NINO, VAT Reg No)
  - Spain (DNI and NIF)
  - Portugal (NIF and NIPC)
  - Canada (SIN and BN)
  - Germany (Steuer-IdNr and USt-IdNr)
  - All other EU countries with a mild validation (for both individual or company)
  - All other World countries with a generic length validation (for both individual or company)

For checking the personal tax code, internally it uses the
[social_security_number](https://github.com/samesystem/social_security_number) gem.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tax_number_validator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tax_number_validator


## Usage

```ruby
  TaxNumberValidator.(
                       type: :personal,
                       country_code: :us,
                       number: '521-70-9465'
                     )
  # => true
```

it returns `true` when number is valid, otherwise `false` when number is invalid
 (doesn't satisfy regexp pattern or math operations, when definded)

### mandatory keys:

  - **type** (symbol or string): can be `personal` or `company`
  - **country_code** (symbol or string): should always be a ISO 3166-1 alpha-2
  (http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).
  - **number** (string): is the tax number to validate.


optional keys:

  - **birth_date** (date or string in format YYYY-MM-DD): can be used for strict
  validation for some personal tax number.
  - **gender** (symbol or string): can be used for strict validation for some
  personal tax number, can be 'male' or 'female'.


## Development

After checking out the repo, run `bin/setup` to install dependencies.  
Then, run `rake test` to run the tests.  
You can also run `bin/console` for an interactive prompt that will allow you to
 experiment.

To install this gem onto your local machine, run `bundle exec rake install`.  
To release a new version, update the version number in `version.rb`, and then
run `bundle exec rake release`, which will create a git tag for the version,
push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## Contributing

Bug reports and pull requests are welcome at
https://bitbucket.org/simplicissimus/tax_number_validator. This project is intended
 to be a safe, welcoming space for collaboration, and contributors are expected
 to adhere to the [Contributor Covenant](http://contributor-covenant.org) code
  of conduct.


## License

The gem is available as open source under the terms of the
[MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the TaxNumberValidator project's codebases, issue trackers,
 chat rooms and mailing lists is expected to follow the
 [code of conduct](https://bitbucket.org/simplicissimus/tax_number_validator/src/master/CODE_OF_CONDUCT.md).
